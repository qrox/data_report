file_list = dir('整治报告/*/*/*.xlsx');

fig_1 = figure('Position', get(0, 'ScreenSize'), 'Color', [1 1 1]);
ax_cap = subplot(2, 3, 1);
ax_res = subplot(2, 3, 4);
ax_vol = subplot(2, 3, 5);
ax_cur = subplot(2, 3, 6);
ax_cap.Position([3 4]) = ax_cur.Position([1 2]) + ax_cur.Position([3 4]) - ax_res.Position([1 2]);
ax_file_1 = axes(fig_1, 'Visible', 'off', 'Units', 'Normalized', 'Position', [0 0 1 1]);

fig_2 = figure('Position', get(0, 'ScreenSize'), 'Color', [1 1 1]);
max_cell_cnt = 24;
fig_2_sub_row = 4;
fig_2_sub_col = 6;
ax_cellvol = gobjects(max_cell_cnt, 1);
for cell_ind = 1 : max_cell_cnt
    ax_cellvol(cell_ind) = subplot(fig_2_sub_row, fig_2_sub_col, cell_ind);
end
ax_suptitle = axes(fig_2, 'Visible', 'off', 'Position', [ax_cellvol(1).OuterPosition(1) ax_cellvol(max_cell_cnt).OuterPosition(2) ...
    (sum(ax_cellvol(max_cell_cnt).OuterPosition([1 3])) - ax_cellvol(1).OuterPosition(1)) ...
    (sum(ax_cellvol(1).OuterPosition([2 4])) - ax_cellvol(max_cell_cnt).OuterPosition(2))]);
suptitle = title(ax_suptitle, '图5-单体电池放电电压曲线', 'Visible', 'on');
ax_file_2 = axes(fig_2, 'Visible', 'off', 'Units', 'Normalized', 'Position', [0 0 1 1]);

plot_cap = gobjects(1, 3);
plot_res = gobjects(1, 2);
plot_vol = gobjects(1, 6);
plot_cur = gobjects(1, 2);
plot_cellvol = gobjects(max_cell_cnt, 3);
plot_file_1 = gobjects();
plot_file_2 = gobjects();
for cell_ind = 1 : max_cell_cnt
    dummydatetime = datetime;
    plot_cellvol(cell_ind, 1) = plot(ax_cellvol(cell_ind), dummydatetime, 0);
    ax_cellvol(cell_ind).NextPlot = 'add';
    ax_cellvol(cell_ind).XLimMode = 'manual';
    ax_cellvol(cell_ind).YLimMode = 'manual';
    plot_cellvol(cell_ind, 2) = plot(ax_cellvol(cell_ind), dummydatetime, 0, 'LineStyle', 'none', 'Marker', 'o', 'MarkerEdgeColor', [1 0 0], 'MarkerFaceColor', [1 0 0]);
    plot_cellvol(cell_ind, 3) = text(ax_cellvol(cell_ind), dummydatetime, 0, '', 'VerticalAlignment', 'top');
    ax_cellvol(cell_ind).Title.String = ['#' num2str(cell_ind)];
    if mod(cell_ind, fig_2_sub_col) == 1
        ax_cellvol(cell_ind).YLabel.String = '电压(V)';
    end
end

errors = cell(0, 2);

actxexcel = actxserver('Excel.Application');
try
    actxexcel.DisplayAlerts = false;
    actxword = actxserver('Word.Application');
catch ex
    actxexcel.Quit();
    actxexcel.delete();
    rethrow(ex);
end

for file_ind = 1 : numel(file_list)
    try
        sheet_dir = fullfile(file_list(file_ind).folder, file_list(file_ind).name);
        disp(sheet_dir);
        file_name = regexp(file_list(file_ind).name, '^(.+)\.xlsx$', 'tokens');
        fig_folder = fullfile(file_list(file_ind).folder, 'pictures');
        fig_dir_1 = fullfile(fig_folder, [file_name{:}{:} '.1.png']);
        fig_dir_2 = fullfile(fig_folder, [file_name{:}{:} '.2.png']);
        report_folder = fullfile(file_list(file_ind).folder, 'reports');
        report_dir = fullfile(report_folder, [file_name{:}{:} '.docx']);

        excelfile = actxexcel.Workbooks.Open(sheet_dir, 0, true);   % UpdateLinks = 0, ReadOnly = true
        try
            pack_data_info = excelfile.Sheets.Item('pack_data_info').UsedRange.Value;
        catch ex
            excelfile.Close();
            rethrow(ex);
        end
        try
            resistance = excelfile.Sheets.Item('resistance').UsedRange.Value;
        catch ex
            resistance = {};
        end
        excelfile.Close();

        for row_ind = size(pack_data_info, 1) : -1 : 1
            if ~all(cellfun(@(c)isempty(c)||(isscalar(c)&&isnan(c)), pack_data_info(row_ind, :)))
                break;
            end
        end
        for col_ind = size(pack_data_info, 2) : -1 : 1
            if ~all(cellfun(@(c)isempty(c)||(isscalar(c)&&isnan(c)), pack_data_info(:, col_ind)))
                break;
            end
        end
        pack_data_info = cell2table(pack_data_info(2 : row_ind, 1 : col_ind), 'VariableNames', pack_data_info(1, 1 : col_ind));
        if iscell(pack_data_info.rcv_time)
            try
                pack_data_info.rcv_time = datetime(pack_data_info.rcv_time);
            catch ex
                pack_data_info.rcv_time = datetime(pack_data_info.rcv_time, 'InputFormat', 'MM/dd/yyyy HH:mm:ss');
            end
        end
        for row_ind = size(resistance, 1) : -1 : 1
            if ~all(cellfun(@(c)isempty(c)||(isscalar(c)&&isnan(c)), resistance(row_ind, :)))
                break;
            end
        end
        for col_ind = size(resistance, 2) : -1 : 1
            if ~all(cellfun(@(c)isempty(c)||(isscalar(c)&&isnan(c)), resistance(:, col_ind)))
                break;
            end
        end
        resistance = resistance(1 : row_ind, 1 : col_ind);

        if ~isempty(resistance)
            if all(cellfun(@ischar, resistance(1, :)))
                res_row_start = 2;
            else
                res_row_start = 1;
            end
            if size(resistance, 2) == 101
                res_mat = cell2mat(resistance(res_row_start : end, 6 : 29));
            elseif size(resistance, 2) == 26
                res_mat = cell2mat(resistance(res_row_start : end, 3 : 26));
            elseif size(resistance, 2) == 100 || size(resistance, 2) == 104
                res_mat = cell2mat(resistance(res_row_start : end, 5 : 28));
            elseif size(resistance, 2) == 99
                res_mat = cell2mat(resistance(res_row_start : end, 4 : 27));
            else
                error('custom:error', 'unknown resistance table format');
            end
            mean_res = mean(res_mat, 1, 'omitnan');
            std_res = std(res_mat, 0, 1, 'omitnan');
            res_sample_cnt = sum(isfinite(res_mat), 1);
            std_res_threshold = min(5, max(1 ./ sqrt(res_sample_cnt) .* 1.001, (res_sample_cnt - 1) ./ sqrt(res_sample_cnt) .* .999));
            res_mat(res_mat < mean_res - std_res .* std_res_threshold | res_mat > mean_res + std_res .* std_res_threshold) = nan;
            mean_res = mean(res_mat, 1, 'omitnan');
        else
            mean_res = [];
        end

        cur_vol_cov = cov(pack_data_info.gen_cur, pack_data_info.gen_vol, 1);
        cur_vol_corr = cur_vol_cov(1, 2) / sqrt(cur_vol_cov(1, 1) * cur_vol_cov(2, 2));
        if cur_vol_corr < -0.8
            error_msg = sprintf('reversed current detected: current-voltage correlation = %g', cur_vol_corr);
            errors(end + 1, [1 2]) = {sheet_dir error_msg}; %#ok<SAGROW>
            warning('custom:warning', '%s', error_msg);
            pack_data_info.gen_cur = -pack_data_info.gen_cur;
        end
        cur = pack_data_info.gen_cur;
        disc_cur = min(0, cur);
        disc_cur_std = sqrt(mean(disc_cur .^ 2));
        delta_cur = diff(cur);
        delta_cur_std = sqrt(mean(delta_cur(2 : end - 1) .^ 2));
        vol = pack_data_info.gen_vol;
        vol_mean = mean(vol);
        vol_std = sqrt(mean(vol .^ 2) - vol_mean .^ 2);
        delta_vol = diff(vol);
        delta_vol_std = sqrt(mean(delta_vol(2 : end - 1) .^ 2));
        disc_start_likelihood = normcdf(disc_cur, 0, disc_cur_std) ...                          % i_k is positive
            .* normcdf(min(0, [0; delta_cur]), 0, delta_cur_std * 2) ...                        % i_k - i_{k-1} is non-negative
            .* normcdf(min(0, [0; delta_vol]), 0, delta_vol_std * 2) ...                        % v_k - v_{k-1} is non-negative
            .* normcdf(-[delta_cur; 0], 0, (delta_cur_std + disc_cur_std) * .5) ...             % i_{k+1} - i_k is negative
            .* normcdf(-[delta_vol; 0], 0, (delta_vol_std + vol_std) * .5) ...                  % v_{k+1} - v_k is negative
            .* normcdf(vol, vol_mean, vol_std) ...                                              % v_k is high
            .* (normcdf(-disc_cur([2 : end, end]), 0, disc_cur_std) - 0.5);                     % i_{k+1} is negative
        disc_end_likelihood = (normcdf(-disc_cur, 0, disc_cur_std) - 0.5) ...                   % i_k is negative
            .* normcdf(min(0, -[0; delta_cur]), 0, delta_cur_std * 2) ...                       % i_k - i_{k-1} is non-positive
            .* normcdf(min(0, -[0; delta_vol]), 0, delta_vol_std * 2) ...                       % v_k - v_{k-1} is non-positive
            .* normcdf([delta_cur; -disc_cur(end)], 0, (delta_cur_std + disc_cur_std) * .5) ... % i_{k+1} - i_k is positive
            .* normcdf([delta_vol; max(vol) - vol(end)], 0, (delta_vol_std + vol_std) * .5) ... % v_{k+1} - v_k is positive
            .* (1 - normcdf(vol, vol_mean, vol_std)) ...                                        % v_k is low
            .* normcdf([disc_cur(2 : end); 0], 0, disc_cur_std);                                % i_{k+1} is positive
        [disc_starts, disc_ends] = meshgrid(1 : numel(cur), 1 : numel(cur));
        valid = disc_ends > disc_starts;
        disc_starts = disc_starts(valid);
        disc_ends = disc_ends(valid);
        disc_interval_likelihood = disc_start_likelihood(disc_starts) .* disc_end_likelihood(disc_ends);
        [~, disc_interval] = max(disc_interval_likelihood);
        disc_start = disc_starts(disc_interval);
        disc_end = disc_ends(disc_interval);

        duration_start = min(disc_start + 1, disc_end - 1);
        disc_duration = (datenum(pack_data_info.rcv_time(disc_end)) - datenum(pack_data_info.rcv_time(duration_start))) * 24;
        pack_actual_disc_cap = -sum(pack_data_info.gen_cur(duration_start : disc_end - 1) .* diff(datenum(pack_data_info.rcv_time(duration_start : disc_end)))) * 24;

        mean_disc_cur = -pack_actual_disc_cap / disc_duration;

        for cell_cnt = max_cell_cnt : -1 : 1
            if ~all(pack_data_info.(['cell_vol_' num2str(cell_cnt)]) == 0)
                break;
            end
        end
        if cell_cnt <= 4
            cell_cnt = 4;
            fig_2_sub_row = 2;
            fig_2_sub_col = 2;
        else
            cell_cnt = 24;
            fig_2_sub_row = 4;
            fig_2_sub_col = 6;
        end
        set(groot(), 'CurrentFigure', fig_2);
        for cell_ind = 1 : max_cell_cnt
            if cell_ind <= cell_cnt
                ax_cellvol(cell_ind).Visible = 'on';
                for plot_ind = 1 : size(plot_cellvol, 2)
                    plot_cellvol(cell_ind, plot_ind).Visible = 'on';
                end
                ax_cellvol(cell_ind) = subplot(fig_2_sub_row, fig_2_sub_col, cell_ind, ax_cellvol(cell_ind));
            else
                ax_cellvol(cell_ind).Visible = 'off';
                for plot_ind = 1 : size(plot_cellvol, 2)
                    plot_cellvol(cell_ind, plot_ind).Visible = 'off';
                end
            end
        end
        disc_cap = nan(cell_cnt, 1);
        disc_end_vol = nan(cell_cnt, 1);
        for cell_ind = 1 : cell_cnt
            disc_end_vol(cell_ind) = pack_data_info.(['cell_vol_' num2str(cell_ind)])(disc_end);
        end
        mean_disc_end_vol = mean(disc_end_vol);
        std_disc_end_vol = std(disc_end_vol);
        cutoff_vol = mean(disc_end_vol(abs(disc_end_vol - mean_disc_end_vol) < std_disc_end_vol * 3));
        for cell_ind = 1 : cell_cnt
            if disc_end_vol(cell_ind) < cutoff_vol
                slant = 0.25;
                cutoff = pack_data_info.(['cell_vol_' num2str(cell_ind)])(duration_start : disc_end) - cutoff_vol .* min(1, (0 : disc_end - duration_start) ./ (disc_end - duration_start) .* slant + 1 - slant)';    % min: remove nans
                if all(cutoff <= 0)
                    disc_cutoff = duration_start;
                else
                    disc_cutoff = find(cutoff(1 : end - 1) >= 0 & cutoff(2 : end) <= 0, 1, 'last') + duration_start;
                end
                if ~isempty(disc_cutoff)
                    disc_cap(cell_ind) = -sum(pack_data_info.gen_cur(duration_start : disc_cutoff - 1) .* diff(datenum(pack_data_info.rcv_time(duration_start : disc_cutoff)))) * 24;
                end
            end
        end
        known = isfinite(disc_cap);
        if sum(known) < 2
            disc_cap(~known) = pack_actual_disc_cap;
        else
            if sum(known) > 2
                C = [disc_end_vol(known) .^ 3, disc_end_vol(known), ones(sum(known), 1)];
            else
                C = [disc_end_vol(known), ones(sum(known), 1)];
            end
            d = disc_cap(known);
            disc_end_vol_max = max(disc_end_vol);
            if sum(known) > 2
                A = [-1,  0,  0; ...
                      0, -1,  0; ...
                      0,  0, -1; ...
                     -cutoff_vol ^ 3, -cutoff_vol, -1; ...
                     disc_end_vol_max ^ 3, disc_end_vol_max, 1];
                 b = [0; 0; 0; -pack_actual_disc_cap; pack_actual_disc_cap * 2];
            else
                A = [-1,  0; ...
                      0, -1; ...
                     -cutoff_vol, -1; ...
                      disc_end_vol_max, 1];
                b = [0; 0; -pack_actual_disc_cap; pack_actual_disc_cap * 2];
            end
            x = lsqlin(C, d, A, b);
            if ~isempty(x)
                if sum(known) > 2
                    C2 = [disc_end_vol(~known) .^ 3, disc_end_vol(~known), ones(sum(~known), 1)];
                else
                    C2 = [disc_end_vol(~known), ones(sum(~known), 1)];
                end
                disc_cap(~known) = C2 * x;
            else
                disc_cap(~known) = pack_actual_disc_cap;
            end
        end
        [sorted_disc_cap, disc_cap_sort_ind] = sort(disc_cap);
        sorted_known = known(disc_cap_sort_ind);

        if isempty(mean_res)
            set(groot(), 'CurrentFigure', fig_1);
            ax_res.Visible = 'off';
            cla(ax_res);
            ax_vol = subplot(2, 2, 3, ax_vol);
            ax_cur = subplot(2, 2, 4, ax_cur);
        else
            set(groot(), 'CurrentFigure', fig_1);
            ax_res.Visible = 'on';
            ax_res = subplot(2, 3, 4, ax_res);
            ax_vol = subplot(2, 3, 5, ax_vol);
            ax_cur = subplot(2, 3, 6, ax_cur);
        end

        plot_num = 1;

        bar_inds = 1 : cell_cnt;
        plot_cap(1, 1) = bar(ax_cap, bar_inds, sorted_disc_cap);
        ax_cap.NextPlot = 'add';
        plot_cap(1, 3) = text(ax_cap, ax_cap.TickLength(1) * max(ax_cap.Position([3 4])) / ax_cap.Position(3) * 2, ...
            1 - ax_cap.TickLength(1) * max(ax_cap.Position([3 4])) / ax_cap.Position(4) * 2, ...
            ['最大容量差：' num2str(max(disc_cap) - min(disc_cap)) 'Ah'], 'Units', 'normalized', 'VerticalAlignment', 'top');
        ax_cap.NextPlot = 'replace';
        ax_cap.XLim = [0 cell_cnt + 1];
        ax_cap.YLim(2) = ax_cap.YLim(2) + diff(ax_cap.YLim) * (2 - sum(plot_cap(1, 3).Extent([2 4]) .* [2 1]));
        ax_cap.XTickMode = 'manual';
        ax_cap.XTickLabelMode = 'manual';
        ax_cap.XTick = 1 : cell_cnt;
        ax_cap.XTickLabel = arrayfun(@(t){num2str(t)}, disc_cap_sort_ind);
        xlabel(ax_cap, '电池序号');
        ylabel(ax_cap, '容量(Ah)');
        title(ax_cap, sprintf('图%d-单体电池容量', plot_num));
        plot_num = plot_num + 1;

        if ~isempty(mean_res)
            plot_res(1, 1) = bar(ax_res, 1 : cell_cnt, mean_res);
            ax_res.NextPlot = 'add';
            plot_res(1, 2) = text(ax_res, ax_res.TickLength(1) * max(ax_res.Position([3 4])) / ax_res.Position(3) * 2, ...
                1 - ax_res.TickLength(1) * max(ax_res.Position([3 4])) / ax_res.Position(4) * 2, ...
                ['最大内阻差：' num2str(max(mean_res) - min(mean_res)) 'm\Omega'], 'Units', 'normalized', 'VerticalAlignment', 'top');
            ax_res.NextPlot = 'replace';
            ax_res.XLim = [0 cell_cnt + 1];
            ax_res.YLim(2) = ax_res.YLim(2) + diff(ax_res.YLim) * (2 - sum(plot_res(1, 2).Extent([2 4]) .* [2 1]));
            ax_res.XTickMode = 'manual';
            ax_res.XTick = 1 : cell_cnt;
            xlabel(ax_res, '电池序号');
            ylabel(ax_res, '内阻(m\Omega)');
            title(ax_res, sprintf('图%d-单体电池内阻', plot_num));
            plot_num = plot_num + 1;
        end

        plot_vol(1, 1) = plot(ax_vol, pack_data_info.rcv_time, pack_data_info.gen_vol);
        ax_vol.NextPlot = 'add';
        plot_vol(1, 2) = scatter(ax_vol, pack_data_info.rcv_time(disc_start), pack_data_info.gen_vol(disc_start), [], [1 0 0], 'filled');
        plot_vol(1, 3) = text(ax_vol, pack_data_info.rcv_time(disc_start), pack_data_info.gen_vol(disc_start), num2str(pack_data_info.gen_vol(disc_start)), 'VerticalAlignment', 'top');
        plot_vol(1, 4) = scatter(ax_vol, pack_data_info.rcv_time(disc_end), pack_data_info.gen_vol(disc_end), [], [1 0 0], 'filled');
        plot_vol(1, 5) = text(ax_vol, pack_data_info.rcv_time(disc_end), pack_data_info.gen_vol(disc_end), num2str(pack_data_info.gen_vol(disc_end)), 'VerticalAlignment', 'top');
        plot_vol(1, 6) = text(ax_vol, ax_vol.TickLength(1) * max(ax_vol.Position([3 4])) / ax_vol.Position(3) * 2, ...
            ax_vol.TickLength(1) * max(ax_vol.Position([3 4])) / ax_vol.Position(4) * 2, ...
            ['放电时长：' num2str(disc_duration) '小时，放电容量：' num2str(pack_actual_disc_cap) 'Ah'], ...
            'Units', 'normalized', 'VerticalAlignment', 'bottom');
        ax_vol.NextPlot = 'replace';
        ax_vol.XLim = [min(pack_data_info.rcv_time) max(pack_data_info.rcv_time)];
        ax_vol.YLim = [ ...
            min([ax_vol.YLim(1), ...
                plot_vol(1, 3).Extent(2), ...
                plot_vol(1, 5).Extent(2)]) ...
            max([ax_vol.YLim(2), ...
                sum(plot_vol(1, 3).Extent([2 4])) + ax_vol.TickLength(1) / max(ax_vol.Position([3 4])) * ax_vol.Position(4) * diff(ax_vol.YLim) * 2, ...
                sum(plot_vol(1, 5).Extent([2 4])) + ax_vol.TickLength(1) / max(ax_vol.Position([3 4])) * ax_vol.Position(4) * diff(ax_vol.YLim) * 2])];
        ax_vol.YLim(1) = ax_vol.YLim(1) - diff(ax_vol.YLim) * sum(plot_vol(1, 6).Extent([2 4]) .* [2 1]);
        ylabel(ax_vol, '电压(V)');
        title(ax_vol, sprintf('图%d-电池组放电电压曲线', plot_num));
        plot_num = plot_num + 1;

        plot_cur(1, 1) = plot(ax_cur, pack_data_info.rcv_time, pack_data_info.gen_cur);
        ax_cur.NextPlot = 'add';
        plot_cur(1, 2) = text(ax_cur, ax_cur.TickLength(1) * max(ax_cur.Position([3 4])) / ax_cur.Position(3) * 2, ...
            ax_cur.TickLength(1) * max(ax_cur.Position([3 4])) / ax_cur.Position(4) * 2, ...
            ['平均放电电流：' num2str(mean_disc_cur) 'A'], 'Units', 'normalized', 'VerticalAlignment', 'bottom');
        ax_cur.NextPlot = 'replace';
        ax_cur.XLim = [min(pack_data_info.rcv_time) max(pack_data_info.rcv_time)];
        ax_cur.YLim(1) = ax_cur.YLim(1) - diff(ax_cur.YLim) * sum(plot_cur(1, 2).Extent([2 4]) .* [2 1]);
        ylabel(ax_cur, '电流(A)');
        title(ax_cur, sprintf('图%d-电池组放电电流曲线', plot_num));
        plot_num = plot_num + 1;

        for cell_ind = 1 : cell_cnt
            set(plot_cellvol(cell_ind, 1), 'XData', pack_data_info.rcv_time, 'YData', pack_data_info.(['cell_vol_' num2str(cell_ind)]));
            set(plot_cellvol(cell_ind, 2), 'XData', pack_data_info.rcv_time(disc_end), 'YData', disc_end_vol(cell_ind));
            if disc_end_vol(cell_ind) < cutoff_vol
                text_color = [.83 .14 0];
            else
                text_color = [0 .14 .83];
            end
            set(plot_cellvol(cell_ind, 3), 'Position', [ruler2num(pack_data_info.rcv_time(disc_end), ax_cellvol(cell_ind).XAxis), disc_end_vol(cell_ind)], 'String', num2str(disc_end_vol(cell_ind)), 'Color', text_color);
            ax_cellvol(cell_ind).XLim = [min(pack_data_info.rcv_time) max(pack_data_info.rcv_time)];
            ax_cellvol(cell_ind).YLimMode = 'auto';
            ax_cellvol(cell_ind).YLim = [ ...
                min(ax_cellvol(cell_ind).YLim(1), ...
                    plot_cellvol(cell_ind, 3).Extent(2) - ax_cellvol(cell_ind).TickLength(1) / max(ax_cellvol(cell_ind).Position([3 4])) * ax_cellvol(cell_ind).Position(4) * diff(ax_cellvol(cell_ind).YLim) * 2) ...
                max(ax_cellvol(cell_ind).YLim(2), ...
                    sum(plot_cellvol(cell_ind, 3).Extent([2 4])) + ax_cellvol(cell_ind).TickLength(1) / max(ax_cellvol(cell_ind).Position([3 4])) * ax_cellvol(cell_ind).Position(4) * diff(ax_cellvol(cell_ind).YLim) * 2)];
            ax_cellvol(cell_ind).YLimMode = 'manual';
        end
        suptitle.String = sprintf('图%d-单体电池放电电压曲线', plot_num);
        plot_num = plot_num + 1;
        
        if ishandle(plot_file_1)
            delete(plot_file_1);
        end
        plot_file_1 = text(ax_file_1, 0, 1, [file_name{:}{:}], 'Visible', 'on', 'Units', 'Normalized', 'VerticalAlignment', 'top');
        if ishandle(plot_file_2)
            delete(plot_file_2);
        end
        plot_file_2 = text(ax_file_2, 0, 1, [file_name{:}{:}], 'Visible', 'on', 'Units', 'Normalized', 'VerticalAlignment', 'top');

        [~, ~, ~] = mkdir(fig_folder);
        [~, ~, ~] = mkdir(report_folder);

        frame = getframe(fig_1);
        [raster, raster_map] = frame2im(frame);
        if isempty(raster_map)
            imwrite(raster, fig_dir_1);
        else
            imwrite(raster, raster_map, fig_dir_1);
        end
        frame = getframe(fig_2);
        [raster, raster_map] = frame2im(frame);
        if isempty(raster_map)
            imwrite(raster, fig_dir_2);
        else
            imwrite(raster, raster_map, fig_dir_2);
        end

        generate_report(actxword, fullfile(pwd, 'template.docx'), report_dir, fig_dir_1, fig_dir_2);
    catch exception
        errors(end + 1, [1 2]) = {sheet_dir getReport(exception, 'extended', 'hyperlinks', 'off')}; %#ok<SAGROW>
        warning(exception.identifier, '%s', getReport(exception));
    end
end

if isempty(errors)
    error_output = {'No error'};
else
    warning('custom:warning', 'Total %d errors', size(errors, 1));
    error_output = errors;
end
try
    excelfile = actxexcel.Workbooks.Add;
    try
        if ~isempty(error_output)
            excelsheet = excelfile.Sheets.Item(1);
            start_cell = excelsheet.Cells.Item(1).Item(1).address;
            end_cell = excelsheet.Cells.Item(size(error_output, 2)).Item(size(error_output, 1)).address;
            excelsheet.Range([start_cell ':' end_cell]).Value = error_output;
        end
        excelfile.SaveAs(fullfile(pwd, 'errors.xlsx'));
    catch ex
        excelfile.Close();
        rethrow(ex);
    end
    excelfile.Close();
catch ex
    warning(ex.identifier, '%s', getReport(ex));
end

try
    actxexcel.Quit();
    actxexcel.delete();
catch ex
    warning(ex.identifier, '%s', getReport(ex));
end
try
    actxword.Quit();
    actxword.delete();
catch ex
    warning(ex.identifier, '%s', getReport(ex));
end
