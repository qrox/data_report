function generate_report(actxword, templatepath, destpath, imgpath1, imgpath2)
    wordfile = actxword.Documents.Open(templatepath);
    try
        wordfile.Bookmarks.Item('image_1').Select();
        actxword.Application.Selection.InlineShapes.AddPicture(imgpath1, true, true);   % (path, link, save_with_doc)

        wordfile.Bookmarks.Item('image_2').Select();
        actxword.Application.Selection.InlineShapes.AddPicture(imgpath2, true, true);

        wordfile.SaveAs2(destpath);
    catch ex
        wordfile.Close();
        rethrow(ex);
    end
	wordfile.Close();
end
