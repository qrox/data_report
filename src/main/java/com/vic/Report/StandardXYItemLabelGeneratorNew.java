package com.vic.Report;

import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.data.xy.XYDataset;

public class StandardXYItemLabelGeneratorNew extends StandardXYItemLabelGenerator {
	
	private int i=3;
	private int i2=5;
	
	
	
	public StandardXYItemLabelGeneratorNew(int i, int i2) {
		super();
		this.i = i;
		this.i2 = i2;
	}



	@Override
    public String generateLabel(XYDataset dataset, int series, int item) {
		String result="";
		if (item==i || item==i2 ) {
			 result=super.generateLabelString(dataset,series,item);
		}
        return result;
    }

}
