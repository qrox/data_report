package com.vic.Report;

class CellCapacity implements Comparable<CellCapacity> {
    private final int ind;
    private final double cap;

    public CellCapacity(int ind, double cap) {
        this.ind = ind;
        this.cap = cap;
    }

    public int compareTo(CellCapacity that) {
        if (this.cap < that.cap) {
            return -1;
        } else if (this.cap > that.cap) {
            return 1;
        } else {
            return 0;
        }
    }

    public int getIndex() {
        return ind;
    }

    public double getCapacity() {
        return cap;
    }
}
