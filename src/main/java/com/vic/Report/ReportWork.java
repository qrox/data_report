package com.vic.Report;

import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints;
import java.io.File;
import java.util.Arrays;

import javax.swing.JFrame;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.TextAnchor;

public class ReportWork {
	
	/** 
     * 解决图表汉字显示问题 
     * @param chart 
     */  
    private static void processChart(JFreeChart chart) {  
        XYPlot plot = chart.getXYPlot();  
        ValueAxis domainAxis = plot.getDomainAxis();  
        ValueAxis rAxis = plot.getRangeAxis();  
        chart.getRenderingHints().put(RenderingHints.KEY_TEXT_ANTIALIASING,  
                RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);  
        TextTitle textTitle = chart.getTitle();  
        textTitle.setFont(new Font("宋体", Font.PLAIN, 20));  
        domainAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 11));  
        domainAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));  
        rAxis.setTickLabelFont(new Font("sans-serif", Font.PLAIN, 12));  
        rAxis.setLabelFont(new Font("宋体", Font.PLAIN, 12));  
        //chart.getLegend().setItemFont(new Font("宋体", Font.PLAIN, 12));  
        // renderer.setItemLabelGenerator(new LabelGenerator(0.0));  
        // renderer.setItemLabelFont(new Font("宋体", Font.PLAIN, 12));  
        // renderer.setItemLabelsVisible(true);  
    }  
	
    //生成折线图
	public void start() {
		try {
			 MysqlConnection mysqlConnection=new MysqlConnection();
			 CellData cellData=mysqlConnection.start();
			 TimeSeries timeSeries=new TimeSeries("Vol Data");
			 Second minute;
			if (cellData.get_cell_vol(1).length==0) {
				System.out.println("无数据");
				return;
			}
			//System.out.println(cellData.get_rcv_time()[cellData.get_cell_vol(1).length-1]);
			 for (int i=0;i<cellData.get_cell_vol(1).length;i++) {
				//	System.out.println(cellData.get_cell_vol(1)[i]);
					minute=new Second(cellData.get_rcv_time()[i]) ;
					timeSeries.add(minute,cellData.get_cell_vol(1)[i]);
				}
			    XYDataset dataset=( XYDataset )new TimeSeriesCollection(timeSeries);
			    //get data ,transform to dataset
			    
		        JFreeChart chart = ChartFactory.createTimeSeriesChart("1", "vol", "time", dataset, false, true, true);  
		        processChart(chart);//change font
		        
		        XYPlot cp = chart.getXYPlot();
		        cp.setBackgroundPaint(ChartColor.WHITE); // 背景色设置  
		        cp.setRangeGridlinePaint(ChartColor.GRAY); // 网格线色设置  
		     
		        
		        XYItemRenderer xyitem = cp.getRenderer();
		      //  xyitem.setSeriesPaint(0, Color.black);//change line's color
		        
                int discharge_interval[] = cellData.estimateDischargeStartAndEnd();
                StandardXYItemLabelGeneratorNew standardXYItemLabelGenerator = new StandardXYItemLabelGeneratorNew(discharge_interval[0], discharge_interval[1]);
		        standardXYItemLabelGenerator.generateLabel(dataset, 0, 5);
		        xyitem.setBaseItemLabelGenerator(standardXYItemLabelGenerator);  
		        xyitem.setBaseItemLabelsVisible(true);
		        xyitem.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_LEFT));
		        //display voltage on charge and discharge point 

                double capacity[] = cellData.estimateCellCapacity(discharge_interval[0], discharge_interval[1]);
		        CellCapacity cell_cap[] = new CellCapacity[cellData.get_cell_cnt()];
		        for (int i = 0; i < cell_cap.length; ++i) {
		        	cell_cap[i] = new CellCapacity(i + 1, capacity[i + 1]);
		        }
		        Arrays.sort(cell_cap);

		        try {  
		        	System.out.println("准备输出文件");
		            ChartUtilities.saveChartAsPNG(new File("VolReport.png"),  
		                    chart, 800, 500);  
		            Histogram(cell_cap);
		            System.out.println("已输出文件");
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        }  
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 
	}
	
	//绘制柱状图
	public  void Histogram(CellCapacity data[]) {
		try {
			 DefaultCategoryDataset dataset = new DefaultCategoryDataset();  
			 for (int i = 0; i < data.length; i++) {
			    dataset.addValue(data[i].getCapacity(), "数据", Integer.toString(data[i].getIndex()));
			}
			
			JFreeChart chart =ChartFactory.createBarChart3D("title", "cell_num", "capacity", dataset, PlotOrientation.VERTICAL, false, false, false);
	        // 从这里开始  
	        CategoryPlot plot = chart.getCategoryPlot();// 获取图表区域对象  
	        
	        // 自定义设定背景色  
	        // chart.setBackgroundPaint(Color.getHSBColor(23,192,223));  
	        chart.setBackgroundPaint(Color.WHITE);  
	        // 获得 plot：3dBar为CategoryPlot  
	        CategoryPlot categoryPlot = chart.getCategoryPlot();  
	        // 设定图表数据显示部分背景色  
	        categoryPlot.setBackgroundPaint(Color.WHITE);  
	        // 横坐标网格线  
	        categoryPlot.setDomainGridlinePaint(Color.GRAY);  
	        // 设置网格线可见  
	        categoryPlot.setDomainGridlinesVisible(true);  
	        // 纵坐标网格线  
	        categoryPlot.setRangeGridlinePaint(Color.GRAY);  
	        // 重要的类，负责生成各种效果  
	        // BarRenderer3D renderer=(BarRenderer3D) categoryPlot.getRenderer();  
	        // 获取纵坐标  
	        NumberAxis numberaxis = (NumberAxis) categoryPlot.getRangeAxis();  
	          
	        // 设置纵坐标的标题字体和大小  
	        numberaxis.setLabelFont(new Font("黑体", Font.CENTER_BASELINE, 16));  
	        // 设置丛坐标的坐标值的字体颜色  
	        numberaxis.setLabelPaint(Color.BLACK);  
	        // 设置丛坐标的坐标轴标尺颜色  
	        numberaxis.setTickLabelPaint(Color.BLACK);  
	        // 坐标轴标尺颜色  
	        numberaxis.setTickMarkPaint(Color.BLUE);  
	        // 丛坐标的默认间距值  
	        // numberaxis.setAutoTickUnitSelection(true);  
	        // 设置丛坐标间距值  
	        numberaxis.setAutoTickUnitSelection(true);  
	        StackedBarRenderer renderer = new StackedBarRenderer();
	      //  renderer.setBaseOutlinePaint(Color.BLACK);
	        renderer.setSeriesPaint(0,Color.GRAY);
	        //重绘格式
	        renderer.setBarPainter(new StandardBarPainter()); 
	        renderer.setShadowVisible(false); 
	        plot.setRenderer(renderer);
	  
	        
	        ChartUtilities.saveChartAsPNG(new File("VolReport2.png"),  
	                chart, 800, 500);  
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
