package com.vic.Report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MysqlConnection {
	final String url="jdbc:mysql://114.55.114.201:1024/ota_service?"
            + "user=yuncai&password=amplifibms&useUnicode=true&characterEncoding=UTF8&useSSL=false";
    private static final int max_cell_cnt = 24;
    private static final String cell_vol_col[];

    static {
        cell_vol_col = new String[max_cell_cnt];
        for (int i = 0; i < max_cell_cnt; ++i) {
            cell_vol_col[i] = "cell_vol_" + (i + 1);
        }
    }
	
	//获取结果数组
	public CellData start() {
		CellData cellData = null;
		Connection conn = null;
		Statement stmt=null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn=DriverManager.getConnection(url);
			stmt=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			String sql="SELECT * FROM base_station_management.pack_data_info WHERE gprs_id='T0B000218' AND rcv_time>'2018-1-27 09:36:36' AND rcv_time<'2018-1-27 14:22:36'";
			ResultSet set=stmt.executeQuery(sql);
			double volDis=0;
			
			set.last();
			int setNum= set.getRow();
			set.first();
			set.previous();
            Date rcv_time[] = new Date[setNum];
            double gen_vol[] = new double[setNum];
            double cell_vol_raw[][] = new double[max_cell_cnt][setNum];
            double gen_cur[] = new double[setNum];

            for (int i = 0; set.next(); ++i) {
                rcv_time[i] = set.getTime("rcv_time");
                for (int cell_ind = 0; cell_ind < cell_vol_raw.length; ++cell_ind) {
                    cell_vol_raw[cell_ind][i] = set.getDouble(cell_vol_col[cell_ind]);
                }
                gen_cur[i] = set.getDouble("gen_cur");
                gen_vol[i] = set.getDouble("gen_vol");
            }
            set.close();

            int non_zero_cnt = lastNonZeroArrayInd(cell_vol_raw) + 1;
            double cell_vol[][];
            if (non_zero_cnt <= 4) {
                int cell_cnt = 4;
                cell_vol = new double[cell_cnt][setNum];
                for (int i = 0; i < cell_vol.length; ++i) {
                    cell_vol[i] = cell_vol_raw[i];
                }
            } else {
                cell_vol = cell_vol_raw;
            }
            cellData = new CellData(rcv_time, gen_vol, cell_vol, gen_cur);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return cellData;
		
	}

    private static int lastNonZeroArrayInd(double arr[][]) {
        boolean all_zero = true;
        int i = arr.length - 1;
        for (; all_zero && i >= 0; --i) {
            for (int k = 0; k < arr[i].length; ++k) {
                if (arr[i][k] != 0) {
                    all_zero = false;
                    break;
                }
            }
        }
        return all_zero ? -1 : i + 1;
    }
}
